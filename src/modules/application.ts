import { createAction } from '@reduxjs/toolkit'

export const loading = createAction('application/loading')