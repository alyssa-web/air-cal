import { Model } from '@nozbe/watermelondb'
import { text, children, date } from '@nozbe/watermelondb/decorators'

export default class Account extends Model {
    static table = 'accounts'
    static associations = {
        transactions: { type: 'has_many', foreignKey: 'transaction_id' },
    }

    @text('name') name
    @children('transactions') transactions
    @date('created_at') createdAt
    @date('updated_at') updatedAt

    // @action async addTransaction(amount) {
    //     return await this.collections.get('transactions').create(transaction => {
    //         transaction.account.set(this)
    //         transaction.amount = amount
    //     })
    // }
}

export const mapAirtableToAccount = ({ id, fields, _rawJson: { createdTime } }) => ({
    id,
    name: fields['Name'],
    created_at: new Date(createdTime).valueOf(),
    updated_at: new Date(fields['Last Updated']).valueOf()
})

export const mapAccountToAirtable = ({ created_at, name, }) => ({
    // fields: {
        Name: name,
    // }
})