import { appSchema, tableSchema } from '@nozbe/watermelondb'

export default appSchema({
  version: 1,
  tables: [
    tableSchema({
        name: 'accounts',
        columns: [
            { name: 'name', type: 'string', isIndexed: true },
            { name: 'created_at', type: 'number' },
            { name: 'updated_at', type: 'number' },
        ]
    }),
    tableSchema({
        name: 'transactions',
        columns: [
            { name: 'name', type: 'string', isIndexed: true },
            { name: 'amount', type: 'number', isIndexed: true },
            { name: 'account_id', type: 'string' },
            { name: 'date', type: 'number', isIndexed: true },
            { name: 'category', type: 'string', isOptional: true },
            { name: 'is_cleared', type: 'boolean' },
            { name: 'created_at', type: 'number' },
            { name: 'updated_at', type: 'number' },
        ]
    }),
    tableSchema({
        name: 'events',
        columns: [
            { name: 'name', type: 'string', isIndexed: true },
            { name: 'rule', type: 'string' },
            { name: 'account', type: 'string', isOptional: true },
            { name: 'created_at', type: 'number' },
            { name: 'updated_at', type: 'number' },
        ]
    }),
    tableSchema({
        name: 'goals',
        columns: [
            { name: 'name', type: 'string' },
            { name: 'target', type: 'number' },
            { name: 'is_complete', type: 'boolean' },
            { name: 'created_at', type: 'number' },
            { name: 'updated_at', type: 'number' },
        ]
    })
  ]
})