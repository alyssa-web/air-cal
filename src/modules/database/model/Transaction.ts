import { Model } from '@nozbe/watermelondb'
import { field, text, children, date, relation } from '@nozbe/watermelondb/decorators'

export default class Transaction extends Model {
  static table = 'transactions'
  static associations = {
    accounts: { type: 'belongs_to', foreignKey: 'account_id' },
  }
  
  @text('name') name
  @field('amount') amount
  @relation('accounts', 'account_id') account
  @date('date') date
  @text('category') category // @relation('category', 'category_id') category
  @field('is_cleared') isCleared
  @date('created_at') createdAt
  @date('updated_at') updatedAt
}