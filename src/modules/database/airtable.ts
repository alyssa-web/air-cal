import { Debugger } from 'debug';
import Airtable from 'airtable';
import TableDef from 'airtable/lib/table';
import Record from 'airtable/lib/record';
import { log } from '../logging/debug';

export const base = new Airtable({
  apiKey: process.env.AIRTABLE_API_KEY,
}).base(process.env.AIRTABLE_BASE_CASHFLOW as string);

export class Table {
    table:TableDef
    log:Debugger

    constructor(table:string) {
        this.table = base(table)
        this.log = log.extend(table)
    }

    // getRecords = (options = {}) =>
    //     this.table.select(options).firstPage()     
    
    getRecords = async (options = {}) => {
        const log = this.log.extend('get')
        log(options)

        // const records = await this.table.select(options).firstPage();
        // const records = await this.table.select(options).all();
        const records = []

        await new Promise((resolve, reject) => {
            this.table.select(options).eachPage((_records, nextPage) => {
                records.push(..._records)
                nextPage()
            }, (err) => {
                if(err) reject(err)
                resolve()
            });
        })        

        log.extend('results')(records);
        return records; 
    }

    getRecord = async (id: string) => {
        const log = this.log.extend('find')
        log(id)

        const record = await this.table.find(id);
        log.extend('result')(record);
        return record;
    }

    /** 
     * Max 10 records at a time
     * 
     * @note Values for Last Updated are automatically computed by Airtable and cannot be directly created. */
    createRecord = async (recordData: Record[] | Record) => {
        const log = this.log.extend('create')
        log(recordData)

        const records = await this.table.create(recordData);
        log.extend('result')(records);
        return records;
    }

    updateRecord = async (recordData: Record[] | Record) => {
        const log = this.log.extend('update')
        log(recordData)

        const records = await this.table.create(recordData);
        log.extend('result')(records);
        return records;
    }

    deleteRecord = async (id: string) => {
        const log = this.log.extend('delete')
        log(id)
        
        const records = await this.table.destroy(id);
        log.extend('result')(records);
        return records;
    }
}