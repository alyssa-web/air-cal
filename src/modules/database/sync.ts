import { synchronize } from '@nozbe/watermelondb/sync'

import { Table } from './airtable';
import { log } from '../logging/debug';
import { database } from './watermelon'
import { format } from 'date-fns';
import { mapAirtableToAccount, mapAccountToAirtable } from './model/Account';
import { syncAction, pull } from '../store/deltas/database.actions';
import { AppDispatch } from '../store';
import { addAccount, updateAccount, removeAccount } from '../store/accounts.duck';

const logger = log.extend('watermelon:sync');

const Accounts = new Table('Accounts')
const Transactions = new Table('Transactions')

export function sync() {
    logger('sync')

    return async (dispatch: AppDispatch) => {
        dispatch(syncAction())

        await synchronize({
            database,
            pullChanges: async ({ lastPulledAt, schemaVersion, migration }) => {
                logger('pull', format(new Date(lastPulledAt), 'M/d/yyyy H:m'), schemaVersion, migration)        
                dispatch(pull(lastPulledAt))

                const accounts = await syncAccounts(lastPulledAt)
                accounts.created.map(account => dispatch(addAccount(account)))
                accounts.updated.map(account => dispatch(updateAccount(account)))
                accounts.deleted.map(account => dispatch(removeAccount(account)))

                // if (!response.ok) {
                //     throw new Error(await response.text())
                // }

                return {
                    changes: {
                        accounts,
                    },
                    timestamp: Date.now().valueOf()
                }
            },
            pushChanges: async ({ changes, lastPulledAt }) => {
                logger('push', changes, lastPulledAt)

                /** can't rely on this as the return results COULD be in a different order than the input */
                // const records = await Accounts.createRecord(changes.accounts.created.map(mapAccountToAirtable))

                // for await (const account of changes.accounts.created.map(record => Accounts.createRecord(mapAccountToAirtable(record)))) {
                //     console.warn(record, account)
                //     debugger
                // }

                for (let index = 0; index < changes.accounts.created.length; index++) {
                    const _account = changes.accounts.created[index];
                    const account = await Accounts.createRecord(mapAccountToAirtable(_account))

                    logger('update id', _account.id, account.id)

                    // const __account = await Accounts.getRecord(account.id)
                    // console.warn(_account, account)
                    // await Accounts.updateRecord()
                    // await __account.updateFields({ id: account.id })
                }

                // console.warn(records)
                // records.forEach(function (record) {
                //     console.log(record.getId());
                // });
            }
        })
    }
}

const syncAccounts = async (lastPulledAt: number) => {
    const records = await Accounts.getRecords({
        // fields: ['Name', 'Last Updated'],
        filterByFormula: `IS_AFTER(LAST_MODIFIED_TIME(),DATETIME_PARSE("${format(new Date(lastPulledAt), 'M/d/yyyy H:m')}"))`,
    })

    // newly created rows have a created date after last sync
    const accountsCreated = records.filter(({ _rawJson: { createdTime } }) => new Date(createdTime).valueOf() >= lastPulledAt).map(mapAirtableToAccount)

    // updated rows have a created date BEFORE last sync
    const accountsUpdated = records.filter(({ _rawJson: { createdTime } }) => new Date(createdTime).valueOf() < lastPulledAt).map(mapAirtableToAccount)

    // any records that don't exist on the server...
    const accountsDeleted = []

    return {
        created: accountsCreated,
        updated: accountsUpdated,
        deleted: accountsDeleted
    }
}