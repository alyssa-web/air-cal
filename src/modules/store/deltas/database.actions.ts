import { createAction } from '@reduxjs/toolkit'

export const syncAction = createAction('database/sync')
export const getTable = createAction<string>('database/getTable')
// READ
export const fetchQuery = createAction('database/fetchQuery')
export const pull = createAction<number>('database/sync/pull')
export const push = createAction('database/sync/push')
export const create = createAction<unknown>('database/row/create')
export const update = createAction<unknown>('database/row/update')
export const deleteRow = createAction<unknown>('database/row/delete')