import { createSlice, PayloadAction, createSelector } from "@reduxjs/toolkit";
import { log } from "../logging/debug";
import { RootState } from ".";

export interface Account {
    id: string,
    name: string
}

const logger = log.extend('accountsSlice');

const initialState: Account[] = []

const accountsSlice = createSlice({
    name: 'accounts',
    initialState,
    reducers: {
        /** @todo handle multiple items */
        add: (state, action) => void(state.push(action.payload)),
        update: (state, action) => {
            console.warn(state, action)
        },
        remove: (state, action) => {
            const index = state.findIndex(({ id }) => id === action.payload)
            if(index > -1) {
                state.splice(index, 1)
            }
        }
    }
})

export const { add: addAccount, update: updateAccount, remove: removeAccount } = accountsSlice.actions

export default accountsSlice.reducer