import { createSlice, createSelector, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from ".";
import { Day } from "./calendar.duck";
import { log } from "../logging/debug";

enum Statuses {
    DONE = 'Done'
}

interface TransactionFields {
    Name: string;
    Amount: number;
    Date: Date;
    Status: Statuses;
}

export interface Transaction {
    id: string;
    fields: TransactionFields;
    createdTime: Date;
}

const logger = log.extend('transactionSlice');

const initialState = JSON.parse(localStorage.getItem('transactions') as string) || [];

const transactionSlice = createSlice({
    name: 'transactions',
    initialState,
    reducers: {
        set: (state, action: PayloadAction<Transaction[]>) => action.payload,

        addTransaction: (state, action: PayloadAction<Transaction>) => {
            // localStorage.setItem('transactions', JSON.stringify([...state, action.payload]));
            return state.push(action.payload);
        },

        toggleTransaction: (state, action: PayloadAction<string>) => {
            const transaction = state.find((transaction: Transaction) => transaction.id === action.payload)
            if(transaction) {
                transaction.confirmed = !transaction.confirmed
            }
        }
    }
})

export const { set: setTransactions, toggleTransaction } = transactionSlice.actions

export const selectTransactions = (state: RootState): Transaction[] => state.transactions

export const selectTransactionsByDay = createSelector(selectTransactions, transactions => {
    return transactions.reduce((prev, next) => {
        let date, idx = prev.findIndex(({ date }) => date === next.fields.Date)
        // logger(idx)

        if(idx > -1) {
            let date = prev[idx]
                date.transactions.push(next)
        } else {
            date = {
                date: next.fields.Date,
                transactions: [next],
                sum: 0,
                balance: 0,
            }
            prev.push(date)
        }
        // logger(date)

        return prev
    }, [] as Day[]).map((day, idx, all) => {
            day.sum = day.transactions.reduce((prev, next) => prev + next.fields.Amount, 0)

            for(let i = 0; i <= idx; i++) {
                day.balance += all[i].sum
            }

            // logger(day.date, day.sum, day.balance)

            return day
        })
})

export const selectConfirmedTransactions = createSelector(selectTransactions, transactions => transactions.filter(transaction => transaction.fields.Status === Statuses.DONE))

export default transactionSlice.reducer