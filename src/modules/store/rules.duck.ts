import { createSlice } from "@reduxjs/toolkit"

import { RRule, RRuleSet, rrulestr } from 'rrule'
import { RootState } from ".";
import { log } from "../logging/debug";

export interface Rule {

}

const logger = log.extend('rulesSlice');

const MaxSalary = new RRule({
    tzid: 'America/Los_Angeles',
    freq: RRule.MONTHLY,
    interval: 1,
    bymonthday: [16, 1],
    // byweekday: RRule.FR, find the closest friday
    wkst: RRule.SU,
    dtstart: new Date(Date.UTC(2018, 2, 15)),
    until: new Date(Date.UTC(2020, 1, 29))
});

const BI_WEEKLY = {
    tzid: 'America/Los_Angeles',
    freq: RRule.WEEKLY,
    interval: 2,
    wkst: RRule.SU,
    byweekday: RRule.FR,
    dtstart: new Date(Date.UTC(2020, 0, 3)),
}

const CgsSalary = new RRule(BI_WEEKLY);

const PlutoSalary = new RRule({
    ...BI_WEEKLY,
    dtstart: new Date(Date.UTC(2020, 2, 13)),
});

const initialState = JSON.parse(localStorage.getItem('rules') as string) || [CgsSalary.toString(), MaxSalary.toString(), PlutoSalary.toString()];

const rulesSlice = createSlice({
    name: 'rules',
    initialState,
    reducers: {
        add: (state, action) => {
            // localStorage.setItem('rules', JSON.stringify([...state, action.payload]));
            return state.push(action.payload);
        },
        // remove: () => 
    }
})

export const { add: addRule } = rulesSlice.actions

export const selectRules = (state: RootState): Rule[] => state.rules

export default rulesSlice.reducer