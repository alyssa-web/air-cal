import { createSlice, PayloadAction, createSelector } from "@reduxjs/toolkit";
import { format, startOfMonth, endOfMonth } from 'date-fns'
import { Transaction } from "./transactions.duck";
import { log } from "../logging/debug";
import { RootState } from ".";

export interface Day {
    date: Date,
    transactions: Transaction[];
    sum: number;
    balance: number;
}

export interface Event {
    id: string,
    title: string,
    date: Date,
    Amount: Number,
}

const DATE_FORMAT = 'yyyy-MM-dd';

const logger = log.extend('calendarSlice');

const TODAY = new Date()

const initialState = {
    /** defaults to today, should not be cached in Local Storage */
    startDate: format(startOfMonth(TODAY), DATE_FORMAT),
    endDate: format(endOfMonth(TODAY), DATE_FORMAT),
    events: [] as Event[]
}

export const selectStartDate = (state: RootState): Date => state.calendar.startDate

export const selectEndDate = (state: RootState): Date => state.calendar.endDate

export const selectEvents = (state: RootState) => state.calendar.events

const calendarSlice = createSlice({
    name: 'calendar',
    initialState,
    reducers: {
        setStartDate: (state, action: PayloadAction<Date>) => void (state.startDate = format(action.payload, DATE_FORMAT)),
        setEndDate: (state, action: PayloadAction<Date>) => void (state.endDate = format(action.payload, DATE_FORMAT)),
        setEvents: (state, action: PayloadAction<Event[]>) => {
            state.events = action.payload
            // state.events.push({
            //     title: action.payload.reduce((curr, { Amount }) => {
            //         return curr + Amount;
            //     }, 0),
            //     date: '2020-01-31',
            //     //   borderColor: '#fff',
            //     //   backgroundColor: '#fff',
            //     //   textColor: '#000',
            //     display: 'background',
            // }
            // )
        }
    }
})

export const { setStartDate, setEndDate, setEvents } = calendarSlice.actions

export default calendarSlice.reducer