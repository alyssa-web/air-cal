import debug from 'debug';

/** __DEBUG__.enable('NAMESPACE'), or __DEBUG__.disable('NAMESPACE') */
window.__DEBUG__ = debug;

process.env.DEBUG && debug.enable(process.env.DEBUG);
export const log = debug('app');
