import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import { format, startOfMonth, endOfMonth, subMonths, addMonths, subDays, addDays } from 'date-fns'
import { RRule, RRuleSet, rrulestr } from 'rrule'
import { log } from '../modules/logging/debug';
import { selectTransactions, setTransactions, toggleTransaction, Transaction, selectTransactionsByDay, } from '../modules/store/transactions.duck';
import { AppDispatch, RootState } from '../modules/store';
import { Day, Event, selectStartDate, selectEndDate, setStartDate, setEndDate, setEvents, selectEvents } from '../modules/store/calendar.duck';
import { selectRules, Rule } from '../modules/store/rules.duck';
import Calendar from './calendar/Calendar';
import { database as localDatabase } from '../modules/database/watermelon'
import { sync } from '../modules/database/sync';

const TRANSACTION_ID = 'rec3yTJtuwrzPDN4Y'

const logger = log.extend('App');
let count = 1

// sync()
//     .then(() => {
//         localDatabase.get('accounts').query().fetch().then(log.extend('Accounts'))
//         localDatabase.get('transactions').query().fetch().then(log.extend('Transactions'))
//         localDatabase.action(async () => {
//             const newAccount = await localDatabase.get('accounts').create(account => {
//                 account.name = 'Test Account 14'
//             })

//             // console.warn(newAccount);

//             sync().then(() => {
//                 localDatabase.get('accounts').query().fetch().then(log.extend('Accounts'))
//             });
//         })
//     })

const MaxSalary = new RRule({
    tzid: 'America/Los_Angeles',
    freq: RRule.MONTHLY,
    interval: 1,
    bymonthday: [16, 1],
    // byweekday: RRule.FR, find the closest friday
    wkst: RRule.SU,
    dtstart: new Date(Date.UTC(2018, 2, 15)),
    until: new Date(Date.UTC(2020, 1, 29))
});

const BI_WEEKLY = {
    tzid: 'America/Los_Angeles',
    freq: RRule.WEEKLY,
    interval: 2,
    wkst: RRule.SU,
    byweekday: RRule.FR,
    dtstart: new Date(Date.UTC(2020, 0, 3)),
}

const CgsSalary = new RRule(BI_WEEKLY);

const PlutoSalary = new RRule({
    ...BI_WEEKLY,
    dtstart: new Date(Date.UTC(2020, 2, 13)),
});

// console.warn(MaxSalary.all())

interface StateProps {
    startDate: Date,
    endDate: Date,
    transactionsByDay: Day[];
    rules: Rule[];
    events: Event[];
}

interface DispatchProps {
    sync: () => void;
    setStartDate: (date: Date) => void;
    setEndDate: (date: Date) => void;
    setEvents: (transactions: Event[]) => void;
    toggle: (id: string) => void;
}

type AppProps = StateProps & DispatchProps

const App = ({ startDate, endDate, transactionsByDay, setEvents, toggle, setStartDate, setEndDate, rules, events, sync }: AppProps) => {
    // logger(`render ${count++}`, transactions)

    useEffect(() => {
        // on first init, dispatch sync
        // then (dispatch) get all accounts

        // sync()
        //     .then(() => {
        //         localDatabase.get('accounts').query().fetch()
        //             .then(log.extend('Accounts'))
        //     })
    }, [])

    useEffect(() => {
        logger('getRecords', startDate, endDate)
        // Transactions.getRecords({
        //     // view: 'Calendar',
        //     filterByFormula: `AND(IS_AFTER({Date}, '${startDate}'),IS_BEFORE({Date}, '${endDate}'))`,
        //     sort: [{ field: "Date", direction: "asc" }]
        // })
        //     .then((transactions) =>
        //         setEvents(transactions.map(({ id, fields: { Date: date, Name: title, Amount } }) => ({
        //             id,
        //             title,
        //             date,
        //             Amount,
        //         }))))

        // Transactions.getRecord(TRANSACTION_ID)
    }, [startDate, endDate])

    return <>
        <Calendar
            events={events}
            datesSet={({ start, end }) => {
                setStartDate(start);
                setEndDate(end);
            }}
            eventContent={({ event }) => `${event.title} - $${event?.extendedProps?.Amount}`
            }
        // dateClick={({ view: { calendar } }) => {
        //     calendar.unselect() // clear date selection
        //     // calendar.addEvent()
        // }} 
        />

        <h2>Rules</h2>
        <ul>
            <li>CGS: {CgsSalary.toText()}</li>
            <li>MAX: {MaxSalary.toText()}</li>
            <li>Pluto TV: {PlutoSalary.toText()}</li>
        </ul>
    </>
}

const mapStateToProps = (state: RootState) => ({
    startDate: selectStartDate(state),
    endDate: selectEndDate(state),
    transactionsByDay: selectTransactionsByDay(state),
    events: selectEvents(state),
    rules: selectRules(state),
});

const mapDispatchToProps = (dispatch: AppDispatch): DispatchProps => ({
    sync: () => dispatch(sync()),
    setStartDate: (date) => dispatch(setStartDate(date)),
    setEndDate: (date) => dispatch(setEndDate(date)),
    setEvents: (transactionEvents) => dispatch(setEvents(transactionEvents)),
    toggle: (transactionId) => dispatch(toggleTransaction(transactionId))
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
