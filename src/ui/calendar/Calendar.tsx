import React from 'react';
import FullCalendar from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction'; // needed for dayClick

import { log } from "../../modules/logging/debug";

const logger = log.extend('Calendar');

const Calendar = (props) => 
    <FullCalendar
        editable={true}
        selectable={true}
        dayMaxEvents={true}
        plugins={[dayGridPlugin, interactionPlugin]}
        initialView="dayGridMonth"
        viewDidMount={logger.extend('View')}
        dateClick={logger.extend('DateClick')}
        eventClick={logger.extend('EventClick')}
        datesSet={(args) => {
          logger.extend('DatesSet')(args);
          logger.extend('DatesSet:currentData')(args.view.getCurrentData()); // viewTitle
        }}
        eventsSet={logger.extend('EventsSet')}
        select={logger.extend('Select')}
        eventAdd={logger.extend('EventAdd')}
        eventChange={logger.extend('EventChange')}
        eventRemove={logger.extend('EventRemove')}
        {...props}
    />

export default Calendar