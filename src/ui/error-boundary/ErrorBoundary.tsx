import React, { Component, ReactNode } from 'react';
import { log } from "../../modules/logging/debug";

const logger = log.extend('ErrorBoundary');
let count = 1

// interface ErrorBoundaryProps {
//   children: ReactNode;
// }

interface ErrorBoundaryState {
  errorMessage: string;
  callStack: string | undefined;
  componentStack?: string;
}

export class ErrorBoundary extends React.Component {
  state = {
    errorMessage: '',
    callStack: '',
    componentStack: '',
  };

  static getDerivedStateFromError({ message, stack }: Error): ErrorBoundaryState {
    // Update state so the next render will show the fallback UI.
    return {
      errorMessage: message,
      callStack: stack,
    };
  }

  componentDidCatch(error: Error, { componentStack }: any): void {
    this.setState({ componentStack });

    // You can also log the error to an error reporting service
    logger(error, { componentStack });
  }

  render(): ReactNode {
    const { errorMessage, callStack, componentStack } = this.state;
    // logger(`render ${count++}`)

    if (errorMessage) {
      // You can render any custom fallback UI
      return (
        <>
          <h1 className="tagline">Something went wrong.</h1>
          <p>{errorMessage}</p>
          <br />
          <pre>
            {callStack}
            <br />
            {componentStack}
          </pre>
        </>
      );
    }

    return this.props.children;
  }
}
