import './modules/logging/why';
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';

import store from './modules/store';
import { ErrorBoundary } from './ui/error-boundary/ErrorBoundary';
import App from './ui/App';

ReactDOM.render(
  <Provider store={store}>
    <ErrorBoundary>
      <App />
    </ErrorBoundary>
   </Provider>,
  document.getElementById('root'),
);

