import { Debug } from 'debug';
import { store } from '../src/modules/store'

declare global {
  interface Window {
    __DEBUG__?: Debug;
    store?: typeof store;
  }
}
